import "./App.css";

import Converter from "./components/converter";

function App() {
  return (
    <div className="App">
      <main>
        <Converter />
      </main>
    </div>
  );
}

export default App;
