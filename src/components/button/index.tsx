import styles from "./index.module.scss";

interface IProps {
  clickEvent: () => void;
  disabled: boolean;
}

const Button: React.FC<IProps> = ({ clickEvent, disabled }) => {
  return (
    <button onClick={clickEvent} disabled={disabled} className={styles.button}>
      Convert
    </button>
  );
};

export default Button;
