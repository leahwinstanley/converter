/* eslint-disable testing-library/prefer-screen-queries */
import { fireEvent, getByRole, render } from "@testing-library/react";
import "@testing-library/jest-dom";

import Button from ".";

describe("button", () => {
  const mockClick = jest.fn();

  it("should render a disabled button", () => {
    const { getByText, container } = render(
      <Button disabled clickEvent={mockClick} />
    );

    expect(getByText("Convert")).toBeInTheDocument();

    fireEvent.click(container);
    expect(mockClick).not.toBeCalled();
  });

  it("should render a not disabled button", () => {
    const { getByText, container } = render(
      <Button disabled={false} clickEvent={mockClick} />
    );

    fireEvent.click(getByRole(container, "button"));

    expect(getByText("Convert")).toBeInTheDocument();
    expect(mockClick).toBeCalled();
  });
});
