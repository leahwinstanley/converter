import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import axios from "axios";

import styles from "./index.module.scss";

interface IProps {
  setCurrency: Dispatch<SetStateAction<string>>;
}

const CountrySelect: React.FC<IProps> = ({ setCurrency }) => {
  const [loading, setLoading] = useState(true);
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const { data: response } = await axios.get(
          "https://openexchangerates.org/api/currencies.json",
          {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              Accept: "application/json",
            },
          }
        );
        setCountries(response);
      } catch (error) {
        console.error(error);
      }
      setLoading(false);
    };

    fetchData();
  }, []);

  let currencyArray = [];

  if (!loading) {
    for (const property in countries) {
      currencyArray.push({ code: property, currency: countries[property] });
    }
  }

  return (
    <select
      name={"currencies"}
      id="currencies"
      form="currencyForm"
      onChange={(e) => setCurrency(e.target.value)}
      className={styles.select}
      data-testid="CurrencySelect"
    >
      {currencyArray?.map((currency, key) => {
        return (
          <option value={currency.code} key={key}>
            {currency.code} | {currency.currency}
          </option>
        );
      })}
    </select>
  );
};

export default CountrySelect;
