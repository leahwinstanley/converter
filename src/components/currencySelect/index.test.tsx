/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/prefer-screen-queries */
import "@testing-library/jest-dom";
import { fireEvent, render } from "@testing-library/react";
import axios from "axios";

import CurrencySelect from ".";

jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe("CurrencySelect", () => {
  const mockSetValue = jest.fn();

  it("should render the select and map through the items that axios returns", () => {
    mockedAxios.get.mockResolvedValueOnce({
      data: {
        A: "A",
        B: "B",
      },
    });

    const { getByTestId } = render(
      <CurrencySelect setCurrency={mockSetValue} />
    );

    const input = getByTestId("CurrencySelect");
    fireEvent.change(input, { target: { value: "B" } });
    expect(input.querySelectorAll("option")).toHaveLength(2);
  });

  it("should render the select and return no options if the get request is empty", () => {
    mockedAxios.get.mockResolvedValueOnce({
      data: {},
    });

    const { getByTestId } = render(
      <CurrencySelect setCurrency={mockSetValue} />
    );

    const input = getByTestId("CurrencySelect");
    expect(input.querySelectorAll("option")).toHaveLength(0);
  });
});
