import { useCallback, useEffect, useRef, useState } from "react";
import axios from "axios";

// Components
import CurrencySelect from "../currencySelect";
import CurrencyInput from "../currencyInput";

import styles from "./index.module.scss";
import Button from "../button";

const Converter = () => {
  const [valueToConvert, setValueToConvert] = useState<number>(0);
  const [startingCurrency, setStartingCurrency] = useState("");
  const [conversionCurrency, setConversionCurrency] = useState("");
  const [rates, setRates] = useState<object>();
  const [convertedValue, setConvertedValue] = useState<number>(0);
  const [error, setError] = useState<string | null>();

  const timeoutRef = useRef(null);

  const convert = useCallback(
    (rates: object | undefined) => {
      if (rates) {
        const keys = Object.keys(rates);
        const requiredIndex = keys.indexOf(conversionCurrency);
        const conversionRate = Object.values(rates)[requiredIndex];
        console.log(rates);

        if (conversionRate) {
          setError(null);
          setConvertedValue(valueToConvert * conversionRate);
        } else {
          setError("Sorry, we couldn't find this rate");
          setConvertedValue(0);
        }
      }
    },
    [valueToConvert, conversionCurrency]
  );

  const getRates = async () => {
    if (startingCurrency) {
      try {
        const { data: response } = await axios.get(
          `https://api.exchangerate-api.com/v4/latest/${startingCurrency}`,
          {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              Accept: "application/json",
            },
          }
        );
        setRates(response?.rates);
      } catch (error) {
        console.error(error);
      }
    }
  };

  useEffect(() => {
    convert(rates);
    const clearConversion = () => {
      setConvertedValue(0);
      setError(null);
    };
    // @ts-ignore
    timeoutRef.current = setTimeout(10000, clearConversion);

    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
    };
  }, [rates, convert]);

  return (
    <div className={styles.converter}>
      <form id="currencyForm">
        <CurrencyInput setValue={setValueToConvert} />
        <CurrencySelect setCurrency={setStartingCurrency} />
        <CurrencySelect setCurrency={setConversionCurrency} />
      </form>
      <Button clickEvent={getRates} disabled={!valueToConvert} />

      {convertedValue !== 0 && (
        <div className={styles.converted}>
          <p>
            {valueToConvert}
            {startingCurrency} is equal to {convertedValue}
            {conversionCurrency}
          </p>
          {/* Ideally here I would add a countdown to show how long this conversion is valid for */}
          <p>Valid for 10 minutes</p>
        </div>
      )}
      {error && (
        <div className={styles.converted}>
          <p>{error}</p>
        </div>
      )}
    </div>
  );
};

export default Converter;
