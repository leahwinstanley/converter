import React, { Dispatch, SetStateAction, useState } from "react";

import styles from "./index.module.scss";

interface IProps {
  setValue: Dispatch<SetStateAction<number>>;
}

const CurrencyInput: React.FC<IProps> = ({ setValue }) => {
  const [currentValue, setCurrentValue] = useState();

  const validateCurrency = (e: { target: { value: any } }) => {
    const currentValue = e.target.value;
    const regex = /^[0-9]*(\.[0-9]{0,2})?$/;
    const checkCurrentValue = currentValue.match(regex);

    if (checkCurrentValue) {
      setCurrentValue(undefined);
      setValue(currentValue);
    } else {
      setCurrentValue(currentValue);
    }
  };

  return (
    <div>
      <label htmlFor="currencyInput" className={styles.label}>
        {currentValue}
      </label>
      <input
        type="number"
        min="1"
        step="any"
        onChange={validateCurrency}
        className={styles.input}
        name="currencyInput"
        id="currencyInput"
        data-testid="currencyInput"
      />
      {currentValue ? (
        <p>Sorry, {currentValue} is not in a valid format</p>
      ) : (
        <p></p>
      )}
    </div>
  );
};

export default CurrencyInput;
