/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/no-container */
/* eslint-disable testing-library/prefer-screen-queries */
import "@testing-library/jest-dom";
import { fireEvent, render } from "@testing-library/react";

import CurrencyInput from ".";

describe("CurrencyInput", () => {
  const mockSetValue = jest.fn();

  it("should render the input and display an error if the value does not match the format", () => {
    const { getByText, getByTestId } = render(
      <CurrencyInput setValue={mockSetValue} />
    );

    const input = getByTestId("currencyInput");
    fireEvent.change(input, { target: { value: 23.004 } });
    expect(
      getByText("Sorry, 23.004 is not in a valid format")
    ).toBeInTheDocument();
  });

  it("should render the input and not display an error if the value is in the correct format", () => {
    const { queryByText, getByTestId } = render(
      <CurrencyInput setValue={mockSetValue} />
    );

    const input = getByTestId("currencyInput");
    fireEvent.change(input, { target: { value: 23.0 } });
    expect(
      queryByText("Sorry, 23.0 is not in a valid format")
    ).not.toBeInTheDocument();
  });
});
